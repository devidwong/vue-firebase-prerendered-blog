# vue-firebase-prerendered-blog

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
## Description

This project contain Vue CLI 3 project wrapped in Webpack. It has three view components. Blog and Article takes posts data from Firebase before render event is fired. The built result are separate static files per route, so every blog post is ready to host on any static hosting.

For now you have to manually put routes to render in .prerender-spa.json file and it should cover Firebase RDB content.
Editing posts in firebase doesnt call build command.

Last step to full SEO is adding vue-meta plugin.
