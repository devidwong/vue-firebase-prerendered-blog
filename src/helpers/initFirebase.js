import firebase from 'firebase'
import firebaseui from 'firebaseui'

const config = {
  apiKey: 'AIzaSyBZ4Gu_5Ni2cfqHlhELgx5WGm7pBoXOHCU',
  authDomain: 'wongbackpress.firebaseapp.com',
  databaseURL: 'https://wongbackpress.firebaseio.com',
  projectId: 'wongbackpress',
  storageBucket: 'wongbackpress.appspot.com',
  messagingSenderId: '857255224900'
}

export const FBApp = firebase.initializeApp(config)
export const FBUIApp = new firebaseui.auth.AuthUI(firebase.auth(FBApp))
export const FBDb = FBApp.database()
