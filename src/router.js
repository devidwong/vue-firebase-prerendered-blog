import Vue from 'vue'
import Router from 'vue-router'
import store from './store'
import Meta from 'vue-meta'
import firebase from 'firebase'
import { FBDb } from './helpers/initFirebase.js'

const Home = () => import('./views/Home.vue')
const Blog = () => import('./views/Blog.vue')
const Article = () => import('./views/Article.vue')
const About = () => import('./views/About.vue')
const List = () => import('./views/List.vue')

Vue.use(Router)
Vue.use(Meta)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/blog',
      name: 'Blog',
      component: Blog,
      beforeEnter: (to, from, next) => {
        function isEmpty (obj) {
          for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
              return false
            }
          }
          return true
        }
        function logOffline () {
          console.log('update timeout')
        }
        if (store.state.blog) {
          console.log('store')
          next()
        } else {
          FBDb.ref('blog').once('value').then((snapshot) => {
            var db = snapshot.val()
            Vue.ls.set('blog', db)
            store.commit('SET_BLOG', db)
            console.log('store updated from db')
            next()
          })
          var ls = Vue.ls.get('blog')
          if (isEmpty(ls)) {
            setTimeout(logOffline, 5000)
          } else {
            if (typeof ls === 'string') {
              ls = JSON.parse(ls)
            }
            store.commit('SET_BLOG', ls)
            console.log('localstorage')
            next()
          }
        }
      }
    },
    {
      path: '/blog/:id',
      name: 'Article',
      props: true,
      component: Article,
      beforeEnter: (to, from, next) => {
        function isEmpty (obj) {
          for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
              return false
            }
          }
          return true
        }
        function logOffline () {
          console.log('update timeout')
        }
        if (store.state.blog) {
          console.log('store')
          next()
        } else {
          FBDb.ref('blog').once('value').then((snapshot) => {
            var db = snapshot.val()
            Vue.ls.set('blog', db)
            store.commit('SET_BLOG', db)
            console.log('store updated from db')
            next()
          })
          var ls = Vue.ls.get('blog')
          if (isEmpty(ls)) {
            setTimeout(logOffline, 5000)
          } else {
            if (typeof ls === 'string') {
              ls = JSON.parse(ls)
            }
            store.commit('SET_BLOG', ls)
            console.log('localstorage')
            next()
          }
        }
      }
    },
    {
      path: '/o-mnie',
      name: 'O mnie',
      component: About
    },
    {
      path: '/list',
      name: 'Lista',
      component: List
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

// Navigation Guard
// TODO Getting currentUser from Vuex to avoid importing firebase
router.beforeEach((to, from, next) => {
  let currentUser = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  if (requiresAuth && !currentUser) next('Hello')
  else next()
})

// this is better place to call render event than mounted() in main Vue instance
router.afterEach((to, from) => {
  document.dispatchEvent(new Event('x-app-rendered'))
})

export default router
