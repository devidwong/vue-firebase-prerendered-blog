import '@babel/polyfill'
import 'es6-promise/auto' // before vuex
import Vue from 'vue'
import Vuefire from 'vuefire'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import { sync } from 'vuex-router-sync'
import VueLocalStorage from 'vue-localstorage'
import './registerServiceWorker'
import { FBApp, FBUIApp, FBDb } from './helpers/initFirebase.js'

Vue.config.productionTip = false

Vue.use(Vuefire)
Vue.use(VueLocalStorage, {
  name: 'ls',
  bind: true
})

sync(store, router)

FBApp.auth().onAuthStateChanged((user) => {
  store.commit('SET_USER', user)
  new Vue({
    ls: {
      blog: {
        type: Object
      }
    },
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
})

store.commit('SET_FB_APP', FBApp)
store.commit('SET_FB_UI_APP', FBUIApp)
store.commit('SET_FB_DB', FBDb)
